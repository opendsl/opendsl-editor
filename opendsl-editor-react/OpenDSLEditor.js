import MonacoEditor from "react-monaco-editor";
import DSLLoader from "./DSLLoader";
import PropTypes from 'prop-types'
import GrammarParser from "./dsl/aviatorscript/grammar/GrammarParser.mjs";

class OpenDSLEditor extends MonacoEditor {

    constructor(props) {
        super(props);
        this.language = props.language;
        this.dslWorker = new DSLLoader().load(this.language);
        this.grammarParser = new GrammarParser();
        this.editor = null;
        this.onChange = props.onChange;
    }

    editorDidMount(editor) {
        this.editor = editor;
        // console.info("editorDidMount:", editor);

        var dslWorker = this.dslWorker;
        var handle = null;
        var onChange = this.onChange;
        editor.onDidChangeModelContent(function() {

            clearTimeout(handle);
            handle = setTimeout(() => {
                //触发内容改变
                onChange(editor.getModel().getValue());
                dslWorker.changeContent(editor);
            }, 500);
        })
    }

    format() {
        var code = this.editor.getValue();
        var newCode = this.dslWorker.format(code);
        this.editor.getModel().setValue(newCode);
    }
}

OpenDSLEditor.propTypes = {
    language: PropTypes.oneOf(['aviatorscript']).isRequired,
    width: PropTypes.string.isRequired,
    height: PropTypes.string.isRequired,
    onChange: PropTypes.func,
}

export default OpenDSLEditor;