# OpenDSL Editor React

#### 介绍
OpenDSL编辑器，React方式封装monaco编辑器，当前只支持AviatorScript脚本语言

#### 软件架构

react + monaco

#### 使用说明

继承于MonacoEditor，添加DSL语法特性封装

```
//引入
import OpenDSLEditor from 'opendsl-editor-react';

//使用
<OpenDSLEditor
    language="aviatorscript"
    height="500"
    width="100%"
    onChange={onChange}
    value={code}
/>
```
更详细的使用见demo源码：../opendsl-editor-react-demo/index.jsx

注意：使用webpack serve正常运行，标准的react项目模式，自动提示不行

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 功能特性

1. 语法高亮（完成）
2. 自动补全（完成）
3. 执行脚本（完成）
4. 语法检查（完成）
5. 代码格式化（完成）
6. VIM模式（完成）

### 访问网站
- 简易版本: http://editor.321zou.com
- 完整版本: http://aviator.321zou.com

### 依赖项目
- https://github.com/react-monaco-editor/react-monaco-editor