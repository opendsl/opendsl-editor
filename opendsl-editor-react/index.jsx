import React from "react";
import { render } from "react-dom";
import OpenDSLEditor from "./OpenDSLEditor";
import { initVimMode } from 'monaco-vim';

var editorRef = React.createRef();
var vimMode = null;
var isVimMode = false;

const App = () => (
    <div>
        <div>
            <h2 style={{textAlign:"center"}}>
                OpenDSL Editor for AviatorScript v0.2.5
            </h2>
        </div>
        <hr />
        <OpenDSLEditor
            language="aviatorscript"
            height="500"
            width="100%"
            onChange={onChange}
            value={code}
            ref={editorRef}
        />
        <div id="vimStatus"></div>
        <br />
        <hr />
        <div style={{ textAlign: "center" }}>
            <button style={{fontSize:"1.3rem"}} onClick={execute} type="button">
                Execute
            </button>
            <span>  </span>
            <button style={{fontSize:"1.3rem"}} onClick={format} type="button">
                Format
            </button>
            <span>  </span>
            <button style={{fontSize:"1.3rem"}} onClick={toggleVimMode} type="button">
                VimMode
            </button>
        </div>
        <hr />
        <div>
            <span style={{fontSize:"1.3rem"}} id="result"></span>
        </div>
    </div>
);

var code = "let message = 'hello, world!';\nprintln(message);";
function onChange(newValue) {
    code = newValue;
}

function execute() {
    fetch("http://aviator.321zou.com/execute/aviator",{
        method:'post',
        headers:{
            'Accept':'application/json,text/plain,*/*',
            'Content-Type':'application/json'
        },
        body:JSON.stringify({code:code})
    }).then((response)=>{
        return response.json()
    }).then((data)=>{
        console.log(data)
        if(data.error) {
            document.getElementById("result").innerHTML ='<span style="color:red">' +  data.error.replace(/\n/g, "<br/>") + "</span>"; 
        } else {
            var output = data.print;
            document.getElementById("result").innerHTML = output.replace(/\n/g, "<br/>");
        }
    }).catch(function(error){
        console.log(error)
        document.getElementById("result").innerHTML ='<span style="color:red">' + JSON.stringify(error) + "</span>"; 
    })
}

function format() {
    editorRef.current.format();
}

function toggleVimMode() {
    if(isVimMode) {
        vimMode.dispose();
    } else {
        var vimStatusNode = document.getElementById('vimStatus');
        vimMode = initVimMode(editorRef.current.editor, vimStatusNode);
    }
    isVimMode = !isVimMode;
}

render(<App />, document.getElementById("root"));
