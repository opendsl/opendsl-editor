import AviatorScript from "./dsl/aviatorscript/AviatorScript";

class DSLLoader {

    constructor() {}

    load(language) {

        var dslWorker = new AviatorScript();

        if ("aviatorscript" === language) {
            dslWorker = new AviatorScript();
        }

        return dslWorker;
    }

}

export default DSLLoader;