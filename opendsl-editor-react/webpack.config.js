const MonacoWebpackPlugin = require("monaco-editor-webpack-plugin");
const path = require("path");

module.exports = {
    entry: "./index.jsx",
    mode: "development",
    devtool: "source-map",
    output: {
        path: path.join(__dirname, "./lib/t"),
        filename: "index.jsx",
    },
    module: {
        rules: [{
                test: /\.html$/,
                use: ["file?name=[name].[ext]"],
            },
            {
                test: /\.(js|jsx|ejs|ts)$/,
                exclude: /node_modules/,
                use: [{
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"],
                        plugins: ["@babel/plugin-proposal-class-properties"],
                    },
                }, ],
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.ttf$/,
                type: 'asset/resource'
            },
        ],
    },
    resolve: {
        extensions: [".js", ".json", ".jsx"],
        // Remove alias until https://github.com/microsoft/monaco-editor-webpack-plugin/issues/68 is fixed
        // alias: { "react-monaco-editor": MonacoEditorSrc }
    },
    plugins: [
        new MonacoWebpackPlugin({
            languages: ["json", "javascript", "typescript"],
        }),
    ],
    devServer: {
        static: "./",
        headers: { 'Access-Control-Allow-Origin': '*' }
    },
    resolve: { fallback: { fs: false } }
};