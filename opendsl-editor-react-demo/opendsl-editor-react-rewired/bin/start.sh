cd $(dirname $0)

cd ..

git pull

# pkill node
pkill tail

lsof -i:3000 | awk '{print $2}' | tail -n 1 | xargs -I {} kill -9 {}

npm install

if [ ! -x logs ]; then
    mkdir logs
fi
npm run start >> logs/server.log &
tail -f logs/server.log &

