## opendsl-editor-react-demo

## 两种方式
- webpack serve: opendsl-editor-react-rewired (src/App.jsx)
- react-app-rewired: opendsl-editor-react-webpack (index.jsx)

react-app-rewired 配置来源说明：
https://github.com/react-monaco-editor/react-monaco-editor#usage-with-create-react-app

### 启动相同
```
npm install
npm run start
```
或者
```
bin/start.sh
```
